const User = require("../Models/userModel");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Login = async (req, res) => {

    const data = req.body;

    const user = await User.findOne({ email: data.email }).exec();

    if (!user) {
        return res.status(401).send("Invalid Email or Password"); 
    }

    const matchPassword = bcrypt.compareSync(data.password, user.password);

    if (matchPassword) {

        const token = jwt.sign({ _id: user._id, email: user.email }, '1922801ab6232ed0528a0fac5b03ba00c5f4d126cdc454ad70bf5a50f36f00c866ec509816800acc39918edea2fc7d9764a26cb5b22734baffc8324673405494', { expiresIn: '1h' });

        res.cookie('token', token, { httpOnly: true });
        res.send("Logged In");
    } else {
        res.status(401).send("Unauthorized Access! Incorrect Password");
    }
};

const VerifyPage= async(req,res) =>{

    if(req.cookies.token){
        try {
            const payload =jwt.verify(req.cookies.token, '1922801ab6232ed0528a0fac5b03ba00c5f4d126cdc454ad70bf5a50f36f00c866ec509816800acc39918edea2fc7d9764a26cb5b22734baffc8324673405494')
            console.log(payload)
            res.send("Logged In")
    }
        catch(error){
            res.status(401).send("Unauthorised Access")
        }
    }
    else{
        res.status(401).send("Unauthorised Access")
    }
}

const verify = async (req, res) =>{
    console.log(req.cookies)
    if(req.cookies.token){
        return res.json({ verified: true })
    }
    else{
        res.json({verified: false})
    }
}

// Logout function
const Logout = async (req, res) => {
    res.cookie('token', '', { httpOnly: true, expires: new Date(0) });
    res.send("Logged Out");
};

module.exports = {
    Login,
    Logout,
    verify
};
