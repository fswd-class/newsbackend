const User = require("../Models/userModel");
const bcrypt = require('bcrypt');
const saltRounds = 10;

const getAllUsers = async (req, res) => {
  res.send('Users!');
};

const getUserbyId = async (req, res) => {
  res.send('User!');
};

const addUser = async (req, res) => {
  const data = req.body;
  const password = req.body.password;
  const hash = bcrypt.hashSync(password, saltRounds);

  const user = new User({ ...data, password: hash });
  await user.save();
  res.json(user);
};

const updateUser = async (req, res) => {
  res.send('Updated User!');
};

const deleteUser = async (req, res) => {
  res.send('Deleted User!');
};

module.exports = {
  getAllUsers,
  getUserbyId,
  addUser,
  updateUser,
  deleteUser
};
