const Author = require('../Models/AuthorModel')

const GetAllAuthors = async (req, res) => {
    const authors = await Author.find({});
    res.json(authors)
}

const GetOneAuthor = async (req, res) => {
    const author = await Author.findById(req.params.AuthorId).exec();
    res.json(author);
}

const AddNewAuthor = async (req, res) => {
    const author= new Author(req.body);
    await author.save();
    res.json(author);
}

const UpdateOneAuthor = async (req, res) => {
    const updateAuthor = await Author.findByIdAndUpdate(req.params.AuthorId, req.body, { new: true });
    res.json(updateAuthor);
}

const DeleteOneAuthor = async (req, res) => {
    await Author.findByIdAndDelete(req.params.AuthorId);
    res.send('Author Deleted!');
}

module.exports = { GetAllAuthors, GetOneAuthor, AddNewAuthor, UpdateOneAuthor, DeleteOneAuthor }