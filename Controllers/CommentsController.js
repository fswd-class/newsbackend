const Comment = require('../Models/CommentsModel');


const GetAllComments = async (req, res) => {
    const comments = await Comment.find();
    res.json(comments);
};


const PostNewComment = async (req, res) => {
    const comment = new Comment(req.body);
    await comment.save();
    res.json(comment);
};


module.exports = { GetAllComments, PostNewComment };