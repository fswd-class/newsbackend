const Article = require('../Models/ArticleModel')

const GetAllArticles = async (req, res) => {
    const category = req.query.category;
    const articles = await Article.find({category: category});
    res.json(articles)
}

const GetOneArticle = async (req, res) => {
    const article = await Article.findById(req.params.ArticleId).exec();
    res.json(article);
}

const AddNewArticle = async (req, res) => {
    const { title, thumbnail, description, category } = req.body;

    try {
        const newArticle = new Article({
            title,
            thumbnail,
            description,
            category
        });

        await newArticle.save();
        res.json(newArticle);
    } catch (err) {
        console.error('Error saving article:', err);
        res.status(500).json({ error: 'Failed to save article' });
    }
}
const UpdateOneArticle = async (req, res) => {
    const updateArticle = await Article.findByIdAndUpdate(req.params.ArticleId, req.body, { new: true });
    res.json(updateArticle);
}

const DeleteOneArticle = async (req, res) => {
    await Article.findByIdAndDelete(req.params.ArticleId);
    res.send('Article Deleted!');
}

module.exports = { GetAllArticles, GetOneArticle, AddNewArticle, UpdateOneArticle, DeleteOneArticle }