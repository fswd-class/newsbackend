const mongoose = require('mongoose');

const ArticlesSchema = new mongoose.Schema({
    title: { type: String, required: true },
    thumbnail: { type: String, required: true },
    description: { type: String, required: true },
    category: {
        type: String,
        required: true,
        enum: ['trendingNow', 'topNews', 'lifestyle', 'sports', 'technology', 'travel']
    },
});
const Article = mongoose.model('Article', ArticlesSchema);

module.exports = Article;