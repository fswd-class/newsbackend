const mongoose = require('mongoose');

const CommentsSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.ObjectId,
        ref: 'User'
    },
    article: {
        type: mongoose.ObjectId,
        ref: 'Article'
    }

});

const Comment = mongoose.model('Comment', CommentsSchema);

module.exports = Comment;