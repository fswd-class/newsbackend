const mongoose = require('mongoose');

const AuthorsSchema = new mongoose.Schema({
    name: String,
    image: String,
    designation: String,
    description: String
});

const Author = mongoose.model('Author', AuthorsSchema);

module.exports = Author;