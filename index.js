const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const ArticlesRoutes = require('./Routes/ArticlesRoutes');
const AuthorsRoutes = require('./Routes/AuthorsRoutes');
const CommentsRoutes = require('./Routes/CommentsRoutes');
const UserRoutes = require('./Routes/UserRoutes');
const cookieParser = require('cookie-parser');
const authRoutes = require('./Routes/AuthRoutes')

const app = express();
const port = 3000;

app.use(cookieParser());
app.use(express.json());
app.use(cors(
  {
    credentials: true,  
    origin: 'http://localhost:5173'
  }
))

// Routes
app.use('/articles', ArticlesRoutes);
app.use('/authors', AuthorsRoutes);
app.use('/comments', CommentsRoutes);
app.use('/users', UserRoutes);
app.use("/auth", authRoutes)



mongoose.connect('mongodb+srv://kabeerahamedkm:3IravmxTNnYZuHRX@cluster0.fijrhjq.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0')
  .then(() => console.log("Connected successfully"))
  .catch(err => console.log(err));


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
