const express = require("express");
const { Login, Logout, verify } = require("../Controllers/AuthController");
const router = express.Router();

// Login route
router.post('/login', Login);
router.get('/verify', verify)

// Logout route
router.get('/logout', Logout);

module.exports = router;

