const express = require("express");
const { getAllUsers, getUserbyId, addUser, updateUser, deleteUser } = require("../Controllers/UserController");
const router = express.Router();

router.get('/', getAllUsers);

router.get('/:userId', getUserbyId);

router.post('/', addUser);

router.patch('/:userId', updateUser);

router.delete('/:userId', deleteUser);

module.exports = router;
