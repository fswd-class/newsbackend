const express = require('express');
const { GetAllComments, PostNewComment} = require('../Controllers/CommentsController')
const {protect} = require ('../Middlewares/Protect')

const router = express.Router();

router.get('/', GetAllComments);
router.post('/', protect, PostNewComment);

module.exports = router;