const express = require('express')
const { AddNewArticle, GetAllArticles, GetOneArticle, UpdateOneArticle, DeleteOneArticle } = require('../Controllers/ArticlesControllers')
const router = express.Router()


router.get('/', GetAllArticles)

router.get('/:ArticleId', GetOneArticle)


router.post('/', AddNewArticle)

router.patch('/:ArticleId', UpdateOneArticle )

router.delete('/:ArticleId', DeleteOneArticle )


module.exports = router