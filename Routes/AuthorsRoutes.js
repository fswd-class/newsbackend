const express = require('express')
const { GetAllAuthors, GetOneAuthor, AddNewAuthor, UpdateOneAuthor, DeleteOneAuthor } = require('../Controllers/AuthorsControllers')
const router = express.Router()


router.get('/', GetAllAuthors)

router.get('/:AuthorId', GetOneAuthor)

router.post('/', AddNewAuthor)

router.patch('/:AuthorId', UpdateOneAuthor )

router.delete('/:AuthorId', DeleteOneAuthor)


module.exports = router